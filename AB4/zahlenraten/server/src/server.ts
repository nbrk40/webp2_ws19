import express = require ('express');

import {Request, Response}  from 'express';   // import from EXPRESS
import { random } from './random';

let randomNumber: number = random(1,10);
let password: string = "passwort";
const app = express();

app.listen(8080, "localhost", function () {
    /*--- Provide some useful information on console  -------------------------*/
    console.log(`
	-------------------------------------------------------------
	  Guessing number webservice - using ajax
	  
	  Server started  in localhost:8080
	  call http://localhost:8080/	
	-------------------------------------------------------------
	`);
});


app.use(express.json());
let baseDir : string = __dirname + '/../..';

app.use("/",       express.static(baseDir + "/client/views/"));
app.use("/src",    express.static(baseDir + "/client/src"));
app.use("/jquery", express.static(baseDir + "/client/node_modules/jquery/dist"));
app.use("/popper.js", express.static(baseDir +"/client/node_modules/popper.js/dist"));
app.use("/bootstrap", express.static(baseDir +"/client/node_modules/bootstrap/dist"));
app.use("/font-awesome", express.static(baseDir + "/client/node_modules/font-awesome/css"));

/**
 * @api {get} /guess/:guess Request number
 * @apiName GetNumber
 * @apiGroup Guess
 * @apiVersion 1.0.0
 *
 * @apiParam {number} get The number that someone guessed
 *
 * @apiSuccess {guess} user The requested user object
 * @apiSuccess {string} message Number was guessed right
 *
 *
 *  @apiError (Error) {404} NumberNotFound The requested number has not been found
 *
 */

app.get("/guess/:guess", (req: Request, res: Response) => {

    let guess : number = Number(req.params.guess);

    if(isNaN(guess)) {
        res.status(404);
        res.json({
            win: false,
            message: 'Please take a guess'
        });


    } else if (( guess > randomNumber)) {
        res.status(404);
        res.json({
            win: false,
            message: 'Your guess was too high'
        });


    } else if((guess < randomNumber)) {
        res.status(404);
        res.json({
            win: false,
            message: 'Your guess was too low'
        });

    } else if ((guess == randomNumber)) {
        res.status(200);
        res.json({
            win: true,
            message: 'You got it!'
        });

    }
});

/**
 * @api {post} /cheat Request password
 * @apiName CheatPassword
 * @apiGroup Cheat
 * @apiVersion 1.0.0
 *
 * @apiParam {number} submit the right password to cheat
 *
 * @apiSuccess {cheat} show random number
 * @apiSuccess {string} message The Number is ___
 *
 *
 *  @apiError (Error) {404} The entered Password is wrong
 *
 */

app.post("/cheat", (req:Request, res:Response) => {
    let enteredPassword: string = String(req.body.password);


    if (password==enteredPassword) {
        res.status(201);
        res.json({
            message: 'The number is '+ randomNumber
        });
    } else{
        res.status(404);
        res.json({
            message: 'Wrong password, try again!'
        });
    }
});

/**
 * @api {post} /reset random Number range
 * @apiName ResetNumber
 * @apiGroup Reset
 * @apiVersion 1.0.0
 *
 * @apiParam {number} reset the range of the random Number
 *
 * @apiSuccess {string} message Reset random number successful
 *
 *
 *  @apiError (Error) {404} message Not all mandotory fields are filled in
 *
 */
app.post("/reset", (req:Request, res:Response) => {
    let min: number = Number(req.body.min);
    let max: number = Number(req.body.max);

    if (min && max) {
        randomNumber = random(min,max);
        res.status(201);
        res.json({
            message: 'Reset random number successful'
        });

    } else {
        res.status(404);
        res.json( {
            message: 'Not all mandotory fields are filled in'
        });
    }
});
