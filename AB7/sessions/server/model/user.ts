import { Rights } from './rights';

// Class representing a user
export class User {
    public id: number;
    public username: string;
    public firstName: string;
    public lastName: string;
    public creationDate: Date;
    public rights: Rights;

    constructor(id: number, username: string, firstName: string, lastName: string, creationDate: Date, rights: Rights) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationDate = creationDate;
        this.rights = rights;
    }
}
