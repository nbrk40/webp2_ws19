# Userman ALL

## Technologies used:

### Server

* [ApiDoc](http://apidocjs.com/)
* [Crypto-JS](https://github.com/brix/crypto-js)
* [MySQL Database](https://www.mysql.com/de/)
* [Sessions](https://github.com/expressjs/session)

### Client

* [Bootstrap](http://getbootstrap.com/)
* [JQuery](https://jquery.com/)

## Project Setup

1. Run `npm install` in both the `client` and `server` directory
1. Setup and import the MySQL database using `userman.sql` 
1. Copy and rename `server/config/config.skeleton` to `server/config/config.ts` and fill in all fields
1. Globally install TSLint using `npm install -g tslint`
1. Enable `TSLint` under `Settings > Languages & Frameworks > TypeScript > TSLint`

## Project Execution

1. Transpile the source TypeScript files in all directories. Run `npm run` to start the app

### Demo Data

There is an admin user account with the following data:
* Username: admin
* Password: admin

### Testing the API

You can use the `userman.http` file to test the api of the server. To do so, just run the Requests given in this file.

Some of the routes need authentication. Therefore, execute the login request and copy the Set-cookie result which 
should look like `<sessionOptions.name>=...`. Set this value as `Cookie` in those requests, which require an active session.
