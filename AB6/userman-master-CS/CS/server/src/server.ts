/*****************************************************************************
 * Import package                                                            *
 *****************************************************************************/
import express = require ('express');
import { Request, Response } from 'express';
import { User } from '../model/user';
import mysql = require('mysql');
import {Connection, MysqlError} from "mysql";


/*****************************************************************************
 * Define global variables                                                   *
 *****************************************************************************/

const database: Connection = mysql.createConnection({
    host: 'localhost',
    port: 3306,
    user: 'root',
    password: '',
    database: 'userman'
});

database.connect((err: MysqlError) => {
    if(err) {
        console.log('Database connection failed:', err);
    } else {
        console.log('Database is connected');
    }
});

//Create data and database query


/*****************************************************************************
 * Define, configure and start web-app server                                *
 *****************************************************************************/
const app = express();
app.use(express.json());
app.listen(8080, () => {
    console.log('Server started: http://localhost:8080');
});

/*****************************************************************************
 * STATIC ROUTES                                                             *
 *****************************************************************************/
const basedir: string = __dirname + '/../..';  // get rid of /server/src
app.use('/', express.static(basedir + '/client/views'));
app.use('/css', express.static(basedir + '/client/css'));
app.use('/src', express.static(basedir + '/client/src'));
app.use('/jquery', express.static(basedir + '/client/node_modules/jquery/dist'));
app.use('/popperjs', express.static(basedir + '/client/node_modules/popper.js/dist'));
app.use('/bootstrap', express.static(basedir + '/client/node_modules/bootstrap/dist'));
app.use('/font-awesome', express.static(basedir + '/client/node_modules/font-awesome'));

/*****************************************************************************
 * HTTP ROUTES: USER, USERS                                                  *
 *****************************************************************************/
/**
 * @api {post} /user Create a new user
 * @apiName postUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {string} firstName First name of the user
 * @apiParam {string} lastName Last name of the user
 *
 * @apiSuccess {string} message Message stating the new user has been created successfully
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully created new user"
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Not all mandatory fields are filled in"
 * }
 */
app.post('/user', (req: Request, res: Response) => {
    // Read data from request body
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;
    // add a new user if first- and lastname exist
    if (firstName && lastName) {
        // Create new user
        let data: [string, string, string] = [
            firstName,
            lastName,
            new Date().toLocaleString()
        ];

        let query: string = 'INSERT INTO userlist (firstName, lastName, creationDate) ' + 'VALUES (?, ?, ?);';

        database.query(query, data, (err: MysqlError, result: any) => {
            if (err) {
                //Query could not be executed
                res.status(500).send({
                    message: 'Database request failed: ' + err,
                });
            } else {
                //The user was created
                res.status(200).send({
                    message: 'Successfully created new user',
                });
            }
        });
    }
});

/**
 * @api {get} /user:userId Get user with given id
 * @apiName getUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the user has been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "user":{
 *         "id":1,
 *         "firstName":"Peter",
 *         "lastName":"Kneisel",
 *         "creationDate":"2018-10-21 14:19:12"
 *     },
 *     "message":"Successfully got user"
 * }
 *
 *  @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The requested user can not be found."
 * }
 */
app.get('/user/:userId', (req: Request, res: Response) => {
    let data: number = Number(req.params.userId);
    let query: string = 'SELECT * FROM userlist WHERE id = ?;';

    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            // Database operation has failed
            res.status(500).send({
                message: 'Database request failed: ' + err
            });
        } else {
            // Check if database response contains exactly one entry
            if (rows.length === 1) {
                res.status(200).send({
                    message: 'Successfully got user',
                    user: new User(
                        rows[0].id,
                        rows[0].firstName,
                        rows[0].lastName,
                        rows[0].groups,
                        new Date(rows[0].creationDate))
                });
            } else {
                res.status(404).send({
                    message: 'The requested user can not be found.'
                });
            }
        }
    });
});

app.get('/groups', (req: Request, res: Response) => {
    const query: string = 'SELECT * FROM group';

    database.query(query, (err: MysqlError, rows: any) => {
        if(err) {
            res.status(500).send({
                message: 'Database request failed: ' + err,
            });
        } else {
            res.status(200).send({
                groups: rows,
                message: 'Successfully requested groups list',
            });
        }
    });
});

/**
 * @api {put} /user/:userId Update user with given id
 * @apiName putUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 * @apiParam {string} firstName The (new) first name of the user
 * @apiParam {string} lastName The (new) last name of the user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully updated user ..."
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Not all mandatory fields are filled in"
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The user to update could not be found"
 * }
 */
app.put('/user/:userId', (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;

    //Create database query
    let data: [string, string, number] = [firstName, lastName, userId];
    let query: string = 'UPDATE userlist SET firstName = ?, lastName = ? WHERE id = ?;';

    //Execute database query
    database.query(query, data, (err:MysqlError, result: any) => {
    if (err) {
    //Query could not been executed
        res.status(500).send({
        message: 'Database request failed:' + err
        });
    } else {
    if (result.affectedRows ===1) {
        //The user was updated
        res.status(200).send({
            message: 'Successfully updated user' + userId
        });
    } else {
        //The user could not be updated
        res.status(404).send({
            message: 'Updating the user failed'
        });
    }}

})}) ;

/**
 * @api {delete} /user/:userId Delete user with given id
 * @apiName deleteUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully deleted user ..."
 * }
 */
app.delete('/user/:userId', (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);

    //Create database query and data
    let data: number = userId;
    let query: string = 'DELETE FROM userlist WHERE id = ?;';

    // Request user from database
    database.query(query, data, (err: MysqlError, result: any) => {
    if (err){
    //Database operation has failed
        res.status(500).send({
        message: 'Database request failed' + err
        });
    } else {
        //Check if database response contains at least one entry
        if(result.affectedRows === 1) {
            res.status(200).send({
            message: 'Successfully deleted user' + userId
            });
        } else {
            //No user found to delete
            res.status(404).send({
            message: 'The requested user can not be deleted'
            });
        }
    }})
});


/**
 * @api {get} /users Get all users
 * @apiName getUsers
 * @apiGroup Users
 * @apiVersion 2.0.0
 *
 * @apiSuccess {User[]} userList The list of all users
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "userList": [
 *      {
 *        "firstName": "Hans",
 *        "lastName": "Mustermann",
 *        "creationDate": "2018-11-04T13:02:44.791Z",
 *        "id": 1
 *     },
 *      {
 *        "firstName": "Bruce",
 *        "lastName": "Wayne",
 *        "creationDate": "2018-11-04T13:03:18.477Z",
 *        "id": 2
 *      }
 *    ]
 *    "message":"Successfully requested user list"
 * }
 */
app.get('/users', (req: Request, res: Response) => {
    //  let data: number = Number(req.params.userId);
    let query: string = 'SELECT * FROM userlist;';
    let userlist: User[] = [];
    let i: number;

    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            // Database operation has failed
            res.status(500).send({
                message: 'Database request failed: ' + err
            });
        } else {
            // Check if database response contains exactly one entry
            for (i = 0; i < rows.length; i++) {
                userlist.push(new User(
                    rows[i].id,
                    rows[i].firstName,
                    rows[i].lastName,
                    rows[i].groups,
                    rows[i].creationDate,
                ));
            }
            res.status(200).send({
                message: 'Succesfully requested user list',
                userList: userlist,
            });
        }
    });
});