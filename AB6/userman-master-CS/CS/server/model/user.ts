// Class representing a user
export class User {

    public id: number;
    public firstName: string;
    public lastName: string;
    public groups: Group[];
    public creationDate: Date;


    constructor(id: number, firstName: string, lastName: string, groups: Group [], creationDate: Date) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groups = groups;
        this.creationDate = creationDate;
    }
}
class Group {

    public id: number;
    public name: string;

    constructor(id: number, name:string) {
        this.id = id;
        this.name= name;
    }
}


