/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/

function simpleReadUsers(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();
    // Perform ajax request
    $.ajax({
        url: '/users',
        type: 'GET',
        dataType: 'json',
        success: (response)      => { renderResult(response) },
        error:   (jqXHRresponse) => { renderResult(jqXHRresponse.responseJSON.message) },
    });
}

function simpleCreateUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const firstNameField: JQuery = $('#create-first-name-input');
    const lastNameField:  JQuery = $('#create-last-name-input');

    // Read values from input fields
    const firstName: string = firstNameField.val().toString().trim();
    const lastName:  string = lastNameField.val().toString().trim();

    // Check if all required fields are filled in
    if (firstName && lastName) {
        $.ajax({
            url: '/user',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({ firstName, lastName }),
            contentType: 'application/json',
            success: (response)      => { renderResult(response) },
            error:   (jqXHRresponse) => { renderResult(jqXHRresponse.responseJSON.message) },
        });
    } else { // Not all required fields are filled in, print error message
        renderMessage('Not all fields are filled. Please check the form');
    }
}

function simpleReadUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();
    // Define JQuery HTML objects
    const idIdInput: JQuery = $('#read-id-input');
    // Read values from input fields
    const userId: number = Number(idIdInput.val().toString().trim());
    // Perform ajax request
    $.ajax({
        url: '/user/' + userId,
        type: 'GET',
        dataType: 'json',
        success: (response)      => { renderResult(response) },
        error:   (jqXHRresponse) => { renderResult(jqXHRresponse.responseJSON.message) },
    });
}

function simpleUpdateUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();
    // Define JQuery HTML objects
    const idIdInput: JQuery = $('#update-id-input');
    const firstNameInput: JQuery = $('#update-first-name-input');
    const lastNameInput: JQuery = $('#update-last-name-input');
    // Read values from input fields
    const userId: number = Number(idIdInput.val().toString().trim());
    const firstName: string = firstNameInput.val().toString().trim();
    const lastName: string = lastNameInput.val().toString().trim();
    // Check if all required fields are filled in
    if (firstName && lastName) {
        $.ajax({
            url: '/user/' + userId,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({ firstName, lastName }),
            contentType: 'application/json',
            success: (response)      => { renderResult(response) },
            error:   (jqXHRresponse) => { renderResult(jqXHRresponse.responseJSON.message) },
        });
    } else { // Not all required fields are filled in, print error message
        renderMessage('Not all fields are filled. Please check the form');
    }
}

function simpleDeleteUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();
    // Define JQuery HTML objects
    const idIdInput: JQuery = $('#delete-id-input');
    // Read values from input fields
    const userId: number = Number(idIdInput.val().toString().trim());
    // Perform ajax request
    // ... not using deprecated "success/error"
    // ... solving wear warning from webstorm concering promises
    $.ajax({
        url: '/user/' + userId,
        type: 'Delete',
        dataType: 'json'
    })
    .done( (response) => { renderResult(response.message) } )
    .fail( (error)    => { renderResult(error.responseJSON.message) } )
    .then( () => {} );
}


/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
function renderResult(result: JSON) {
    $('#result').html(JSON.stringify(result, null, 2));
}


/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
    $('#read-users-form').on ('submit', simpleReadUsers);
    $('#create-form').on     ('submit', simpleCreateUser);
    $('#read-form'  ).on     ('submit', simpleReadUser);
    $('#update-form').on     ('submit', simpleUpdateUser);
    $('#delete-form').on     ('submit', simpleDeleteUser);
});
