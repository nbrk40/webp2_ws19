/*****************************************************************************
 * Class and enum declaration                                                *
 *****************************************************************************/
// Class representing a user

class Animal {

    public id: number;
    public name: string;
    public yearOfBirth: number;
    public gender: string;

    constructor(name: string, yearOfBirth: number, gender: string) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.gender = gender;
    }
}

class User {
    private static userCounter: number = 1;

    public id: number;
    public firstName: string;
    public lastName: string;
    public pet: Animal;
    public creationDate: Date;

    constructor(firstName: string, lastName: string, pet: Animal, creationDate: Date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.pet = pet;
        this.creationDate = creationDate;
        this.id = User.userCounter++;

    }
}



let userList: User[] = [];

/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/
function addUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const firstNameField: JQuery = $('#add-first-name-input');
    const lastNameField: JQuery = $('#add-last-name-input');
    const petNameField: JQuery = $('#add-pet-name-input');
    const petYearField: JQuery = $('#add-year-pet-input');
    const petGenderField: JQuery = $('#add-gender-pet-input');


    // Read values from input fields
    const firstName: string = firstNameField.val().toString().trim();
    const lastName: string = lastNameField.val().toString().trim();
    const petName: string = petNameField.val().toString().trim();
    const petYear: number = Number(petYearField.val());
    const petGender: string = petGenderField.val().toString().trim();




    // Check if all required fields are filled in
    if (firstName && lastName && petName) {
        // Create new user
        const pet: Animal = new Animal (petName, petYear, petGender);
        const user: User = new User(firstName, lastName, pet, new Date());
        user.pet = pet;
        // Add user to user und pet list
        userList.push(user);
        // Show changes on website
        renderMessage('User created');
        addUserForm.trigger('reset');
        renderUserList();

    } else if (firstName && lastName) {
        const user: User = new User(firstName, lastName, null, new Date());
        // Add user to user list
        userList.push(user);
        // Show changes on website
        renderMessage('User created');
        addUserForm.trigger('reset');
        renderUserList();
    }
        else {
        renderMessage('Not all mandatory fields are filled in');
    }

}

function editUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');
    const firstNameInput: JQuery = $('#edit-first-name-input');
    const lastNameInput: JQuery = $('#edit-last-name-input');
    const idHiddenInput: JQuery = $('#edit-id-input');

    // Read values from input fields
    const userId: number = Number(idHiddenInput.val().toString().trim());
    const firstName: string = firstNameInput.val().toString().trim();
    const lastName: string = lastNameInput.val().toString().trim();

    if (firstName && lastName) {
        // iterate through userList until user found (or end of list)
        for (const user of userList) {
            if (user.id === userId) {
                user.firstName = firstName;
                user.lastName = lastName;
                // Show success message and changes
                renderMessage(`Successfully updated user ${user.firstName} ${user.lastName}`);
                editUserForm.trigger('reset');
                renderUserList();
                editModal.modal('hide');
                return;  // leave function when found
            }
        }
        // The user could not be found, show error message
        renderMessage('The user to update could not be found');
    } else {
        renderMessage('Not all mandatory fields are filled in');
    }
    editModal.modal('hide');
}

function deleteUser(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    for (let user of userList) {
        if (user.id == userId) {
            userList = userList.filter((user) => user.id !== userId);
            user.pet = null;
        }
    }

    renderMessage('User deleted');
    renderUserList();
}


function deletePet(event) {

    const userId: number = $(event.currentTarget).data('user-id');

    for(let user of userList) {
        if (user.id == userId) {
            user.pet = null;
        }
    }

    renderMessage('Pet deleted');
    renderUserList();
}

function SearchFor() {

    const userTableBody: JQuery = $('#user-table-body tr');
    const value: string = $(this).val().toString().toLowerCase();
    userTableBody.filter(function():any {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
    
}


function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    const editFirstNameInput: JQuery = $('#edit-first-name-input');
    const editLastNameInput: JQuery = $('#edit-last-name-input');

    // iterate through list until user found (or end of list)
    for (const user of userList) {
        if (user.id === userId) {
            // Fill in edit fields in modal
            editIdInput.val(user.id);
            editFirstNameInput.val(user.firstName);
            editLastNameInput.val(user.lastName);
            // Show modal
            editUserModal.modal('show');
            renderUserList();
            return;  // leave function when found
        }
    }
    renderMessage('The selected user can not be found');
}

/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');

    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

    // Add message to DOM
    messageWindow.append(newAlert);

    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(() => {
        newAlert.alert('close');
    }, 5000);
}

function renderUserList() {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#user-table-body');

    // Delete the old table of users from the DOM
    userTableBody.empty();
    // For each user create a row and append it to the user table

    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-2" data-user-id="${user.id}" >
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button mr-2" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                    ${user.pet ? `<button class="btn btn-outline-dark btn-sm delete-pet-button" data-user-id="${user.id}">
                        <i class="fa fa-paw" aria-hidden="true"></i>
                    </button>` : "" }
                </td>
            </tr>
        `);

        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

function ShowPets() {
    //Define JQuery HTML objects
    const statsModal: JQuery = $('#stats-modal');
    const one: JQuery = $('#one');
    const two: JQuery = $('#two');
    const three: JQuery = $('#three');

    //Pet Counter
    let petCount: number = 0;
    for (let user of userList) {
        if (user.pet) {
            petCount++
        }
    }

    //Oldest Pet
    let currentYear: number = 2019;
    userList.sort((a, b) => a.pet.yearOfBirth - b.pet.yearOfBirth);
    let oldestPet = userList [0];

    let petAge = currentYear - oldestPet.pet.yearOfBirth;


    //Average Age of Pets
    let sum: number = 0;
    let i: number = 0;
    for (let user of userList) {
        if (user.pet) {
            sum+= Number(user.pet.yearOfBirth);
            i++
        }
    }

    sum = sum/i;
    sum = currentYear-sum;

    //Add to Modal Window
    one.text(petCount);
    two.text(petAge);
    three.text(sum);

    //Show Modal Window
    statsModal.modal('show');

}

function sortUpID() {
    userList.sort((a,b) => b.id - a.id);
    renderUserList();
}

function sortDownID() {
    userList.sort((a,b) => a.id - b.id);
    renderUserList();
}

function sortUpFirst() {
    userList.sort((a, b) => b.firstName.localeCompare(a.firstName));
    renderUserList();
}

function sortDownFirst() {
    userList.sort((a,b) => a.firstName.localeCompare(b.firstName));
    renderUserList();
}

function  sortUpLast() {
    userList.sort((a,b) => b.lastName.localeCompare(a.lastName));
    renderUserList();
}

function sortDownLast() {
    userList.sort((a,b) => a.lastName.localeCompare(b.lastName));
    renderUserList();
}


/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const editUserForm: JQuery = $('#edit-user-form');
    const userTableBody: JQuery = $('#user-table-body');
    const searchInput: JQuery = $('#search');
    const idUp: JQuery = $('#idUp');
    const idDown: JQuery = $('#idDown');
    const fUp: JQuery = $('#fUp');
    const fDown: JQuery = $('#fDown');
    const lUp: JQuery = $('#lUp');
    const lDown: JQuery = $('#lDown');
    const stats: JQuery = $('#stats');


    // Register listeners
    addUserForm.on('submit', addUser);
    editUserForm.on('submit', editUser);
    userTableBody.on('click', '.edit-user-button', openEditUserModal);
    userTableBody.on('click', '.delete-user-button', deleteUser);
    userTableBody.on('click', '.delete-pet-button', deletePet);
    searchInput.on('keyup', SearchFor);
    idUp.on('click', sortUpID);
    fUp.on ('click',sortUpFirst);
    lUp.on ('click',sortUpLast);
    idDown.on('click', sortDownID);
    fDown.on('click', sortDownFirst);
    lDown.on('click', sortDownLast);
    stats.on('click', ShowPets);

});


